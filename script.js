const musicContainer = document.getElementById('music-container');

const playBtn = document.getElementById('play');
const prevBtn = document.getElementById('prev');
const nextBtn = document.getElementById('next');

const audio = document.getElementById('audio');
const progress = document.getElementById('progress');
const progressContainer = document.getElementById('progress-container');
const volume = document.getElementById('volume');
const volumeContainer = document.getElementById('volume-container');

const title = document.getElementById('title');
const cover = document.getElementById('cover');

const songs = ['Bob Marley - One Love', 'Calm Piano Music', 'Nango Manchay - Space Inbetween'];

let songIndex = 0;

loadSong(songs[songIndex]);

function loadSong(song) {
  title.innerText = song;
  audio.src = `Music/${song}.mp3`;
  cover.src = `Images/${song}.jpg`;
  audio.volume = 0.5;
}

function playSong() {
  musicContainer.classList.add('play');
  playBtn.querySelector('i.fas').classList.remove('fa-play');
  playBtn.querySelector('i.fas').classList.add('fa-pause');

  audio.play();
}

function pauseSong() {
  musicContainer.classList.remove('play');
  playBtn.querySelector('i.fas').classList.add('fa-play');
  playBtn.querySelector('i.fas').classList.remove('fa-pause');

  audio.pause();
}

playBtn.addEventListener('click', () => {
  const isPlaying = musicContainer.classList.contains('play');

  if (isPlaying) {
    pauseSong();
  } else {
    playSong();
  }
});

function updateProgress(eventParam) {
  const { duration, currentTime } = eventParam.srcElement;
  const progressPercent = (currentTime / duration) * 100;
  progress.style.width = `${progressPercent}%`;
}

function updateVolume(eventParam) {
  const timeStamp = eventParam.srcElement;
  const volumePercent = timeStamp.volume * 100;
  volume.style.width = `${volumePercent}%`;
  const volumeIcon = volumeContainer.querySelector('i.fa');

  if (volumePercent > 80) {
    volumeIcon.classList.remove('fa-volume-off');
    volumeIcon.classList.remove('fa-volume-down');
    volumeIcon.classList.add('fa-volume-up');
    volumeIcon.opacity = 1;
  }
  if (volumePercent <= 10) {
    volumeIcon.classList.remove('fa-volume-up');
    volumeIcon.classList.add('fa-volume-off');
    volumeIcon.opacity = timeStamp.volume * 1.2;
  } else if (volumePercent > 10 && volumePercent <= 70) {
    volumeIcon.classList.remove('fa-volume-off');
    volumeIcon.classList.remove('fa-volume-up');
    volumeIcon.classList.add('fa-volume-down');
  }
  volume.style.opacity = timeStamp.volume;
  volumeIcon.opacity = timeStamp.volume;
  console.log(volumePercent);
}

function nextSong() {
  songIndex++;
  if (songIndex <= songs.length - 1) {
    loadSong(songs[songIndex]);
    playSong();
  } else {
    songIndex = 0;
    loadSong(songs[songIndex]);
    playSong();
  }
}

nextBtn.addEventListener('click', () => {
  nextSong();
});

function setProgress(e) {
  const width = this.clientWidth;
  const clickX = e.offsetX;
  const duration = audio.duration;

  audio.currentTime = (clickX / width) * duration;
}

function setVolume(e) {
  const width = this.clientWidth;
  const clickX = e.offsetX;

  audio.volume = clickX / width;
}

prevBtn.addEventListener('click', () => {
  if (songIndex >= 0) {
    loadSong(songs[songIndex--]);
    playSong();
  } else {
    songIndex = songs.length - 1;
  }
});

audio.addEventListener('timeupdate', updateProgress);

audio.addEventListener('volumechange', updateVolume);

progressContainer.addEventListener('click', setProgress);

volumeContainer.addEventListener('click', setVolume);

audio.addEventListener('ended', nextSong);
